// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	velocidad.x=0;
	velocidad.y=0;
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	y1=y1+velocidad.y*t;
	y2=y2+velocidad.y*t;
}

bool Raqueta::Rebota(Esfera &e)
{
	Vector2D dir;
	float dif=Distancia(e.centro,&dir)-e.radio;
	if(dif<=0.0f)
	{
		Vector2D v_inicial=e.velocidad;
		e.velocidad=v_inicial-dir*2.0*(v_inicial*dir);
		e.centro=e.centro-dir*dif;
		return true;
	}
	return false;
}
